## [1.0.16](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.15...v1.0.16) (2020-11-06)


* Merge branch 'develop' into 'master' ([09f964e](https://gitlab.com/tapioca/treinamento/simple-api/commit/09f964e75f778df643fb77ecdb35388d1f6c2617))
* Merge branch 'hotfix/issue-007' into 'develop' ([4c9708f](https://gitlab.com/tapioca/treinamento/simple-api/commit/4c9708f290c7692059d17e639a2aa54a2189c2e9))


### Bug Fixes

* correção da issue 007 ([429d5e5](https://gitlab.com/tapioca/treinamento/simple-api/commit/429d5e544f485a5f513fee9ef36a6323af16918e))
* correção do pipeline ([2462278](https://gitlab.com/tapioca/treinamento/simple-api/commit/2462278f9aa18108dc5db5a7ea99e635a4143c86))


### Code Refactoring

* change index.js location ([4421e4c](https://gitlab.com/tapioca/treinamento/simple-api/commit/4421e4cd59e01c3ca89d2737f7cf19a4c299cfd3))

## [1.0.15](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.14...v1.0.15) (2020-11-04)


* Merge branch 'develop' into 'master' ([0bb6cc2](https://gitlab.com/tapioca/treinamento/simple-api/commit/0bb6cc209650a5fbdefd4209d3fb600070c42398))
* Merge branch 'hotfix/fix-issue-005' into 'develop' ([104afed](https://gitlab.com/tapioca/treinamento/simple-api/commit/104afede73941ca9e54440e2ac9ad59c82d74b8a))
* Merge branch 'hotfix/fix-issue-005' of https://gitlab.com/tapioca/treinamento/simple-api into hotfix/fix-issue-005 ([6ed03c9](https://gitlab.com/tapioca/treinamento/simple-api/commit/6ed03c91d666e578f460474723a6ad2346f0bff1))
* Merge branch 'hotfix/1.0.1' into develop ([cc0ba13](https://gitlab.com/tapioca/treinamento/simple-api/commit/cc0ba13952d68e5ff8bf8b5f746f50f0df2b0fa1))


### Bug Fixes

* add install spet before package ([3044ca9](https://gitlab.com/tapioca/treinamento/simple-api/commit/3044ca9aed729626a944e8d433bc4f049d636350))
* deploy to dev ([61677fb](https://gitlab.com/tapioca/treinamento/simple-api/commit/61677fb6d33ab973deccb5da4c581bbe1880b46b))
* fix issue 004 ([08998fc](https://gitlab.com/tapioca/treinamento/simple-api/commit/08998fcbca7cfa51fcecaade50e37b9ba35bf6f5))
* fix issue 004 ([4cea16d](https://gitlab.com/tapioca/treinamento/simple-api/commit/4cea16d467cf71297fda9abde080ca6500bfec70))

## [1.0.14](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.13...v1.0.14) (2020-10-27)


### Bug Fixes

* fix issue [#004](https://gitlab.com/tapioca/treinamento/simple-api/issues/004) ([c7ec72f](https://gitlab.com/tapioca/treinamento/simple-api/commit/c7ec72f7b196acec2f11f75cee86348ac8eea5cc))


### Code Refactoring

* simplificação do pipeline ([039198c](https://gitlab.com/tapioca/treinamento/simple-api/commit/039198ce3c6e0dcccc26f04ca291c861b115fdf6))
* simplificação do pipeline 2 ([4eeed50](https://gitlab.com/tapioca/treinamento/simple-api/commit/4eeed509676c647d4a43305dff3c818090816685))

## [1.0.13](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.12...v1.0.13) (2020-10-27)


* Merge branch 'hotfix/fix-issue-002' into 'master' ([061157b](https://gitlab.com/tapioca/treinamento/simple-api/commit/061157b5abf21ce32abe92a8738bd161a7849553))


### Bug Fixes

* fix issue [#003](https://gitlab.com/tapioca/treinamento/simple-api/issues/003) ([889270f](https://gitlab.com/tapioca/treinamento/simple-api/commit/889270f16ae4ccc31a92e2c985176bef75e5d794))
* fix spell check ([4490946](https://gitlab.com/tapioca/treinamento/simple-api/commit/449094696157d41b14d248f1d9b636b936e97366))


### Code Refactoring

* spell check ([0dfee58](https://gitlab.com/tapioca/treinamento/simple-api/commit/0dfee589c0c030964f2089ff7e2bedf5cc34bd91))

## [1.0.12](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.11...v1.0.12) (2020-10-27)


* Merge branch 'master' of https://gitlab.com/tapioca/treinamento/simple-api ([09cfe13](https://gitlab.com/tapioca/treinamento/simple-api/commit/09cfe1301330dba3613484f0a4657069112a23f2))


### Bug Fixes

* fixing issue [#002](https://gitlab.com/tapioca/treinamento/simple-api/issues/002) ([f3d7d91](https://gitlab.com/tapioca/treinamento/simple-api/commit/f3d7d91237c436e8152afe4d3d7f8ea8f0ca24dd))

## [1.0.11](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.10...v1.0.11) (2020-10-26)


### Bug Fixes

* ajuste na etapa de deploy do pipeline ([b9eb67e](https://gitlab.com/tapioca/treinamento/simple-api/commit/b9eb67e3ab308fd984b092d161d6887503272d5d))
* ajuste na etapa de deploy do pipeline ([df20cd8](https://gitlab.com/tapioca/treinamento/simple-api/commit/df20cd838180bb077d78b8b2b60c7e49591a8342))
* ajuste na etapa de deploy do pipeline ([b502046](https://gitlab.com/tapioca/treinamento/simple-api/commit/b50204699944c50d53f06ed2724f9960e08b7bd2))
* fixing issue [#001](https://gitlab.com/tapioca/treinamento/simple-api/issues/001) ([b27df6c](https://gitlab.com/tapioca/treinamento/simple-api/commit/b27df6c5a826ff03d7565ee6e5533ae30666b898))
* fixing issue [#001](https://gitlab.com/tapioca/treinamento/simple-api/issues/001) ([ae0bc26](https://gitlab.com/tapioca/treinamento/simple-api/commit/ae0bc266e9932920ce8ac0587c7417f571f4bf84))


### Code Refactoring

* simplificação do pipeline ([8e20aa7](https://gitlab.com/tapioca/treinamento/simple-api/commit/8e20aa7e0380bf24c89c292aa4348910b07e259e))
* simplificação do pipeline ([7f1d61d](https://gitlab.com/tapioca/treinamento/simple-api/commit/7f1d61dcb3b839183b4b32070460b7433d41cb9e))
* simplificação do pipeline ([17fa235](https://gitlab.com/tapioca/treinamento/simple-api/commit/17fa235baf90b7018b67cab6677b5474de61872e))
* simplificação do pipeline ([39c852f](https://gitlab.com/tapioca/treinamento/simple-api/commit/39c852f41611fca6bdecec54168c53e4d0c7ce9d))
* simplificação do pipeline ([8eeec38](https://gitlab.com/tapioca/treinamento/simple-api/commit/8eeec3826d8a5ab60c6602f73c99e5e91f272600))
* simplificação do pipeline ([df83522](https://gitlab.com/tapioca/treinamento/simple-api/commit/df83522c9ea52a8d9005bc44d780cc076038b028))

## [1.0.10](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.9...v1.0.10) (2020-10-26)


* Merge branch 'master' of https://gitlab.com/tapioca/treinamento/simple-api ([3b06ff3](https://gitlab.com/tapioca/treinamento/simple-api/commit/3b06ff3511e2777f44a087d2e5b52285d8221b13))


### Bug Fixes

* alteração para execução de deploy rodar no branch master ([b4ec75f](https://gitlab.com/tapioca/treinamento/simple-api/commit/b4ec75fe2477f554c694a8c2e3f44aad28a017c0))

## [1.0.9](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.8...v1.0.9) (2020-10-26)


### Code Refactoring

* adição de etapa de deploy para staging no pipeline ([272ec11](https://gitlab.com/tapioca/treinamento/simple-api/commit/272ec11bfa33fa93516200517beb1aa5a0eeb0b8))

## [1.0.8](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.7...v1.0.8) (2020-10-26)


### Bug Fixes

* ajusta pipeline para pegar versão correta do sistema na criação da imagem ([7df3665](https://gitlab.com/tapioca/treinamento/simple-api/commit/7df3665460ee37de8e428dfa1ce71cf6230ffed5))

## [1.0.7](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.6...v1.0.7) (2020-10-26)


* Merge branch 'master' of https://gitlab.com/tapioca/treinamento/simple-api ([8460844](https://gitlab.com/tapioca/treinamento/simple-api/commit/8460844d30a01542fbf2edd8aba53b10601c886e))


### Code Refactoring

* melhorias no pipeline v7 ([9ad33c4](https://gitlab.com/tapioca/treinamento/simple-api/commit/9ad33c45b26a95db100b945ade45783f2bb18673))

## [1.0.6](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.5...v1.0.6) (2020-10-26)


* Merge branch 'master' of https://gitlab.com/tapioca/treinamento/simple-api ([1531c1a](https://gitlab.com/tapioca/treinamento/simple-api/commit/1531c1a892db82f2a976d7e82a8d04bf2dfd8951))


### Code Refactoring

* melhorias no pipeline v6 ([fc2a39e](https://gitlab.com/tapioca/treinamento/simple-api/commit/fc2a39e5fd15cac8532bdd464dd4fd472c949a8d))

## [1.0.5](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.4...v1.0.5) (2020-10-26)


* Merge branch 'master' of https://gitlab.com/tapioca/treinamento/simple-api ([cd15887](https://gitlab.com/tapioca/treinamento/simple-api/commit/cd1588738a4165e25711b25278d4407c3c694689))


### Code Refactoring

* melhorias no pipeline v5 ([3b6f6af](https://gitlab.com/tapioca/treinamento/simple-api/commit/3b6f6af4b01afd5c56fddb85c4ac0b7726428be3))

## [1.0.4](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.3...v1.0.4) (2020-10-26)


### Code Refactoring

* melhorias no pipeline ([f4106f3](https://gitlab.com/tapioca/treinamento/simple-api/commit/f4106f3567c3e9f8348cdfd2e0e0a3e4fc7ca282))
* melhorias no pipeline ([cdb3514](https://gitlab.com/tapioca/treinamento/simple-api/commit/cdb35142a25a454bfa4514b2d0e80e85801acc37))
* melhorias no pipeline v3 ([62c5b0b](https://gitlab.com/tapioca/treinamento/simple-api/commit/62c5b0b63cf67252d169ef43c012903863fe54ce))
* melhorias no pipeline v4 ([4631979](https://gitlab.com/tapioca/treinamento/simple-api/commit/46319793e656a820e96f74ed55e6302435b0f532))

## [1.0.3](https://gitlab.com/tapioca/treinamento/simple-api/compare/v1.0.2...v1.0.3) (2020-10-22)


### Bug Fixes

* fix pipeline error ([3d7f0a0](https://gitlab.com/tapioca/treinamento/simple-api/commit/3d7f0a09fb8d4b24c3c94aedfbc3a2987deed7c3))


### Code Refactoring

* adding better pipeline ([a1830fd](https://gitlab.com/tapioca/treinamento/simple-api/commit/a1830fde26c0673a5cc64979683a44383071f168))
* adding better pipeline ([646d68e](https://gitlab.com/tapioca/treinamento/simple-api/commit/646d68e6a1fdd02b335623dc3ac845f900e7a064))
* just testing ([d40826e](https://gitlab.com/tapioca/treinamento/simple-api/commit/d40826e1bcbd1393af23da31aa269a59bbb27aaf))
* **gitlab:** just a simple refactoring ([26ed2db](https://gitlab.com/tapioca/treinamento/simple-api/commit/26ed2dbdbdc003d38b84a843634fde0a4676a51d))


* Lançando versão 1.0.4 ([259ea14](https://gitlab.com/tapioca/treinamento/simple-api/commit/259ea146918c0f552b47344b86e9bea08d810534))
* Lan;cando versão 1.0.3 ([bf2ddb4](https://gitlab.com/tapioca/treinamento/simple-api/commit/bf2ddb4575f19d3f671009d9c96f533830c4a73e))
